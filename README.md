# yaddnsc: Yet Another Dynamic DNS Client
A CLI tool for updating your dynamic DNS records.

## Features
* Supports IPv4 and IPv6 (when the underlying DDNS service supports both)
* Supports forcing of either IPv4 or IPv6 detection
* Supports webhook notifications on successful updates, no change, and error events

## Supported Services
| Type      | Description |
| ---       | ---         |
| [cloudflare](https://gitlab.com/ddb_db/yaddnsc-cloudflare)| Cloudflare DNS |
| [dyndns](https://gitlab.com/ddb_db/yaddnsc-dyndns)| DynDNS v2/v3; any service that claims support for DynDNS v2/v3 protocol |

## Requirements
* Grab a prebuilt binary for your operating system
  * If your OS is not available then post an issue and ask for your system binary
  * If you can't wait that long, download the source and build the binary for your platform
* Run the command with no arguments to see a list of options

## Installation
For Linux/Unix OSes, you likely just want to setup a cronjob that calls this tool periodically.

For Windows, the easiest approach is to call this from a Scheduled Task.

## Examples
### Show Supported DNS Services
`yaddnsc -services`

```
Type                             Description                                    
========================================================================================
cloudflare                       Cloudflare DNS (https://cloudflare.com)        
```

**Note:** This example may not be current.  See [Supported Services](#supported-services) for a current list or run the latest version with the `-services` flag.

### Update Using IP Detected from Internet
The most common configuration is to setup a cronjob on a vm somewhere on your LAN and detect your IP via an HTTP request to an external site.

#### config.json
```
{
  "services": [
    {
      "type": "cloudflare",
      "config": {
        "token": "My-Secret-Cloudflare-Token",
        "zone_id": "My-DNS-Zone-ID",
        "hosts": [
          "myhost.mydomain.com",
          "mydomain.com"
        ]
      }
    }
  ]
}
```

#### Command
`yaddnsc -config config.json`

### Detecting IP from System NIC
On a router, you will usually want to grab the IP from a local NIC.  First, get a list of all known interfaces on the system:

`yaddnsc -interfaces`

```
lo, eth0, eth1, eth0.1001
```

Then tell `yaddnsc` to fetch the current IP from an interface.

```
{
  "interface": "eth1",
  "services": [
    {
      "type": "cloudflare",
      "config": {
        "token": "My-Secret-Cloudflare-Token",
        "zone_id": "My-DNS-Zone-ID",
        "hosts": [
          "myhost.mydomain.com",
          "mydomain.com"
        ]
      }
    }
  ]
}
```

`yaddnsc -config config.json`

The first non-private, globally routable IPv4 and IPv6 addresses are used.  Add `forceIPv4: true` and `forceIPv6: true` options to force IPv4 or IPv6, respectively.

### Hardcoding IP addresses
Finally, you can also just override the detected IP address.  You would usually only do this on a manual run to force an update to a specifc IP.  The option is an
array of max size two and only one IPv4 and one IPv6 address is allowed (i.e. one of each when specifying two IPs in the array).

```
{
  "myip": [
    "100.4.8.2",
    "fe81:1::"
  ],
  "services": [
    {
      "type": "cloudflare",
      "config": {
        "token": "My-Secret-Cloudflare-Token",
        "zone_id": "My-DNS-Zone-ID",
        "hosts": [
          "myhost.mydomain.com",
          "mydomain.com"
        ]
      }
    }
  ]
}
```

`yaddnsc -config config.json`

### Loop Mode
You can run yaddnsc in a long running loop.  This would be ideal for a docker container, for example.  (Yes, I'll be creating docker builds very soon.)

In this mode, the config file is loaded **once** and executed in a never ending loop periodically (default is every 15 minutes).

```
{
  "loopMode": true,
  "loopMinutes": 5
  "services": [
    {
      "type": "cloudflare",
      "config": {
        "token": "My-Secret-Cloudflare-Token",
        "zone_id": "My-DNS-Zone-ID",
        "hosts": [
          "myhost.mydomain.com",
          "mydomain.com"
        ]
      }
    }
  ]
}
```

`yaddnsc -config config.json`

## Configuration
The config file is a json object.  The object includes a key named `services`.  This is an array of objects, each object is an entry to be processed based
on the detected IP of the current run.

The format of the configuration is formally defined via a [JSON schema](https://json-schema.org/) in `config-schema.json`.  The only difference between
supported service types is the contents of the `config` object within each element of `services`.  The requirements of this object differ from system
to system.  The good news is that every system supported in `yaddnsc` **must** formally describe the required format of this `config` object.  Run
`yaddnsc` with the `-schema` option to get a dump of all supported systems and their required `config` format.  For a more human friendly description of
each config object, visit the service's project page (links in the table above).

### Integration
As one can imagine, the `-schema` option provides a very nice programmatic description of the exact requirements to configure `yaddnsc` and any of
its supported services.  A router's webui might find this very valuable, making `yaddnsc` a viable candiate as a router platform's DDNS client.

**These schemas are enforced at runtime by yaddnsc**.  When you reference the schemas and generate files that adhere to them, they are guaranteed to work, without
question.  Again, such configuration and data schemas make `yaddnsc` a very promising choice for any platform's DDNS client.

## Events
The event handlers are optional and define webhooks that will be hit when each event is encoutered.  When an entry specifies multiple hosts, events are processed for each host.

* `onStart`: This event fires just prior to the DDNS service call being made.  Use this event to trigger a metrics call or to signal the start of a timer, etc.  There are no variable substitutions available for this event.  Any variable references will resolve to `<no value>`.
* `onError`: This event fires anytime something goes wrong with the processing.  This can be an app error, network error, service error, etc.
* `onChange`: This event fires when an IP change was detected and the DNS record was updated successfully with the serivce.
* `onNoChange`: This event fires when all checks completed successfully but no IP change was detected (the IP in the service's DNS was the same as the detected IP of the system)
* `onSuccess`: This event fires when onChange or onNoChange fires.  Use this when you want to be notified of a successful run whether the IP changed or not.

```
{
    "services": [
        {
            "type": "cloudflare",
            "config": {
                // snipped for brevity
            },
            "onSuccess": [{       # webhook for onSuccess event
                "url": "https://mydomain.com/abcdefg",        # (required) url to hit
                "method": "POST",                             # (required) http method [get|put|post|patch|delete]
                "contentType": "application/json",            # content type of request; application/json assumed if not specified
                "headers": {                                  # custom headers; Content-Type and User-Agent are ignored
                    "X-Custom-Header": "my header value",
                    "X-Customer-2": "another header value"
                },
                "queryParams": { # query params to add to the request
                    "x": "a",
                    "y": "b",
                    "z": "c"
                },
                "body": { # json encoded body; ignored for GET requests; contentType must be application/json or text/plain and this body is always encoded to json
                    "foo": "bar",
                    "zoo": "jar"
                }
            }]
        },
        {...},
        {...}
    ]
}
```

The `onStart`, `onError`, `onChange`, and `onNoChange` entries were not included, but are configured exactly the same as `onSuccess` in the example above.

### Webhook Variable Substitution
In your webhook configuration, you can use variables that will allow you to provide contextual information for each webhook call that is made.

The variables differ depending on the DDNS service you are using.  Like their required configuration options, each supported service must also
describe the details of what they return for variable substitution.  The output of the `-schema` flag also includes a schema describing the
variable substution structure.  An example for Cloudflare is shown below.

#### Example
```
{
    "services": [
        {
            "type": "cloudflare",
            "config": {
                // snipped for brevity
            },
            "onChange": [{
                "url": "https://somedomain.com/abcdefg",
                "method": "POST",
                "contentType": "application/json",
                "body": {
                    "old": "{{ .OriginalIP }}",
                    "new": "{{ .UpdatedIP }}",
                    "host": "{{ .Host }}",
                    "status": "{{ .Status }}",
                    "zone_id": "{{ .ZoneID }}"
                }
            }]
        }
    ]
}
```

Variable substitution can be done in any of the fields of the webhook definition.  So you can do substitutions in headers, query params, body, url, etc.
Again, the available variables to substitute differ depending on the DDNS service you are using.  See the docs for the service you are using for 
details on the available variables.

## Logging
All logging is done to stdout.  The default log level is `INFO`.  This is very quiet and only logs messages when an error is encountered or if an IP
address change was detected.  This log level is suitable for use in cronjobs where output is only produced when something interesting occurs.

If you only want the tool to output when something goes wrong (i.e. an error is encountered) then set the log level to 'ERROR'.  At this level, the tool
will not log detected IP changes.

To see the details of what this tool is doing, set the level to `DEBUG`.  This is very noisy and will produce output on every run.  You probably don't want
to set this level when calling from a cronjob.

**WARNING:** When the log level is set to `DEBUG`, secrets like API tokens will **ALWAYS** be logged on every run.  Be careful when using debug logging.
