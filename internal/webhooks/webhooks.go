/*
yaddnsc
Copyright (C) 2023  Derek Battams <derek@battams.ca>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package webhooks

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"
	"text/template"

	"github.com/davecgh/go-spew/spew"
	"github.com/go-resty/resty/v2"
	"github.com/vargspjut/wlog"
	"gitlab.com/ddb_db/yaddnsc/internal/config"
	"gitlab.com/ddb_db/yaddnsc/internal/dbg"
	"gitlab.com/ddb_db/yaddnsc/internal/httputils"
)

type Service interface {
	Compile(req Request, name string, vars map[string]any) (Request, error)
	QuickSend(url string)
	Send(req Request)
}

type Request struct {
	URL         string
	Method      string
	ContentType string
	Headers     map[string]string
	QueryParams map[string]string
	Body        map[string]any
}

type defaultImpl struct {
	cfg   config.Settings
	utils httputils.HTTPUtils
}

type Option func(impl *defaultImpl)

func WithConfig(cfg config.Settings) Option {
	return func(impl *defaultImpl) {
		impl.cfg = cfg
	}
}

func WithHTTPUtils(utils httputils.HTTPUtils) Option {
	return func(impl *defaultImpl) {
		impl.utils = utils
	}
}

func New(opts ...Option) Service {
	impl := &defaultImpl{}
	for _, o := range opts {
		o(impl)
	}
	if impl.cfg == nil {
		impl.cfg = config.Get()
	}
	if impl.utils == nil {
		impl.utils = httputils.New(httputils.WithConfig(impl.cfg))
	}
	return impl
}

func (impl *defaultImpl) Compile(req Request, name string, vars map[string]any) (Request, error) {
	enc, err := json.MarshalIndent(req, "", "  ")
	if err != nil {
		return req, fmt.Errorf("webhooks: webhook marshal failed: %w", err)
	}
	compiled := string(enc)

	tmpl, err := template.New(name).Parse(compiled)
	if err != nil {
		wlog.WithScope(wlog.Fields{"err": err.Error(), "tmpl": compiled}).Error("webhooks: tmpl parse failed")
		return req, fmt.Errorf("webhooks: tmpl parse failed: %w", err)
	}

	buf := bytes.Buffer{}
	if err := tmpl.Execute(&buf, vars); err != nil {
		wlog.WithScope(wlog.Fields{"err": err.Error(), "tmpl": compiled}).Error("webhooks: tmpl exec failed")
		return req, fmt.Errorf("webhooks: tmpl execute failed: %w", err)
	}

	compiledReq := Request{}
	if err := json.Unmarshal(buf.Bytes(), &compiledReq); err != nil {
		wlog.WithScope(wlog.Fields{"err": err.Error(), "tmpl": compiled}).Error("webhooks: tmpl unmarshal failed")
		return req, fmt.Errorf("webhooks: tmpl unmarshal failed: %w", err)
	}

	return compiledReq, nil
}

func (impl *defaultImpl) QuickSend(url string) {
	req := Request{
		URL:    url,
		Method: "GET",
	}
	impl.Send(req)
}

func (impl *defaultImpl) Send(req Request) {
	wlog.Debugf("Attempting webhook:\n%s", spew.Sdump(req))
	http := impl.buildClient(req)
	r := http.R()
	configHTTPReq(r, req)
	resp, err := r.Execute(strings.ToUpper(req.Method), "")
	defer impl.utils.LogRestyTrace(resp)
	if err != nil {
		wlog.WithScope(wlog.Fields{"url": req.URL, "err": err.Error()}).Error("webhooks: send failed")
	} else if resp.IsError() {
		body := resp.Body()
		data := "[none]"
		if body != nil {
			enc, err := json.Marshal(body)
			if err == nil {
				data = string(enc)
			}
		}
		wlog.WithScope(wlog.Fields{"url": req.URL, "status": resp.Status(), "data": data}).Error("webhooks: send error")
	} else {
		wlog.WithScope(wlog.Fields{"url": req.URL, "status": resp.Status(), "data": string(resp.Body())}).Debug("webhook completed")
	}
}

func configHTTPReq(r *resty.Request, req Request) {
	method := strings.ToUpper(req.Method)
	if method == "POST" || method == "PUT" || method == "PATCH" {
		if req.Body != nil {
			enc, err := json.Marshal(req.Body)
			if err != nil {
				panic(err)
			}
			r.SetBody(string(enc))
		} else {
			wlog.Warningf("webhooks: no body specified for %s %s", method, req.URL)
		}
	} else if req.Body != nil {
		wlog.Warningf("webhooks: body ignored for %s %s", method, req.URL)
	}
}

func (impl *defaultImpl) buildClient(req Request) *resty.Client {
	http := resty.New()
	http.SetBaseURL(req.URL)
	http.SetHeaders(req.Headers)
	http.SetHeader("Content-Type", req.ContentType)
	http.SetHeader("User-Agent", dbg.GetUserAgent())
	http.SetQueryParams(req.QueryParams)
	if impl.cfg.IsLogHTTP() {
		http.EnableTrace()
	}
	return http
}
