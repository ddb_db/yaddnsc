/*
yaddnsc
Copyright (C) 2023  Derek Battams <derek@battams.ca>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package httputils

import (
	"github.com/go-resty/resty/v2"
	"github.com/vargspjut/wlog"
	"gitlab.com/ddb_db/yaddnsc/internal/config"
	"gitlab.com/ddb_db/yaddnsc/internal/dbg"
)

type HTTPUtils interface {
	GetClient() *resty.Client
	LogRestyTrace(resp *resty.Response)
}

type defaultImpl struct {
	cfg  config.Settings
	http *resty.Client
}

type Option func(impl *defaultImpl)

var instance HTTPUtils

func New(opts ...Option) HTTPUtils {
	if len(opts) == 0 && instance != nil {
		return instance
	}

	impl := &defaultImpl{}
	for _, o := range opts {
		o(impl)
	}
	if impl.cfg == nil {
		impl.cfg = config.Get()
	}
	if impl.http == nil {
		impl.http = buildClient(impl.cfg)
	}

	if len(opts) == 0 {
		instance = impl
	}
	return impl
}

func buildClient(cfg config.Settings) *resty.Client {
	client := resty.New()
	client.SetHeader("User-Agent", dbg.GetUserAgent())
	if wlog.GetLogLevel() == wlog.Dbg && cfg.IsLogHTTP() {
		client.EnableTrace()
	}
	return client
}

func WithConfig(cfg config.Settings) Option {
	return func(impl *defaultImpl) {
		impl.cfg = cfg
	}
}

func WithClient(http *resty.Client) Option {
	return func(impl *defaultImpl) {
		impl.http = http
	}
}

func (impl *defaultImpl) GetClient() *resty.Client {
	return impl.http
}

func (impl *defaultImpl) LogRestyTrace(resp *resty.Response) {
	if !impl.cfg.IsLogHTTP() || wlog.GetLogLevel() != wlog.Dbg {
		return
	}
	trace := resp.Request.TraceInfo()
	fields := wlog.Fields{
		"restyResult":        resp.Result(),
		"restyErr":           resp.Error(),
		"restyHttpCode":      resp.StatusCode(),
		"restyHttpStatus":    resp.Status(),
		"restyProto":         resp.Proto(),
		"restyDuration":      resp.Time().String(),
		"restyRecdAt":        resp.ReceivedAt(),
		"restyDnsLookup":     trace.DNSLookup.String(),
		"restyConnTime":      trace.ConnTime.String(),
		"restyTCPConnTime":   trace.TCPConnTime.String(),
		"restyTLSHandsTime":  trace.TLSHandshake.String(),
		"restyTTFB":          trace.ServerTime.String(),
		"restyRespTime":      trace.ResponseTime.String(),
		"restyTotalTime":     trace.TotalTime.String(),
		"restyIsConnReused":  trace.IsConnReused,
		"restyIsConnWasIdle": trace.IsConnWasIdle,
		"restyReqAttempt":    trace.RequestAttempt,
		"restyRemoteAddr":    trace.RemoteAddr.String(),
		"restyTarget":        resp.Request.RawRequest.URL.String(),
		"restyMethod":        resp.Request.Method,
		"restyAgent":         resp.Request.RawRequest.UserAgent(),
	}
	if impl.cfg.IsLogHTTPBodies() {
		fields["restyRespBody"] = string(resp.Body())
		// TODO log headers
	}
	wlog.WithScope(fields).Debug("http request")
}
