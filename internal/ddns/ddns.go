/*
yaddnsc
Copyright (C) 2023  Derek Battams <derek@battams.ca>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package ddns

import (
	"encoding/json"
	"fmt"
	"net"
	"path"

	"github.com/vargspjut/wlog"
	"gitlab.com/ddb_db/yaddnsc/internal/appmgr"
	"gitlab.com/ddb_db/yaddnsc/internal/config"
	"gitlab.com/ddb_db/yaddnsc/internal/registry"
	"gitlab.com/ddb_db/yaddnsc/internal/validator"
	"gitlab.com/ddb_db/yaddnsc/internal/webhooks"
)

type Ddns interface {
	Execute(int, appmgr.Request, net.IP) error
}

type defaultImpl struct {
	reg       registry.Registry
	processor appmgr.ResponseProcessor
	cfg       config.Settings
	hooks     webhooks.Service
	log       wlog.Logger
}

func New(reg registry.Registry, processor appmgr.ResponseProcessor, cfg config.Settings, hooks webhooks.Service, log wlog.Logger) Ddns {
	return &defaultImpl{
		reg:       reg,
		processor: processor,
		cfg:       cfg,
		hooks:     hooks,
		log:       log,
	}
}

func (impl *defaultImpl) Execute(idx int, req appmgr.Request, ip net.IP) error {
	entry, err := json.MarshalIndent(req.Config, "", "  ")
	if err != nil {
		return fmt.Errorf("ddns: json marshal failed: %w", err)
	}
	action := impl.reg.GetService(req.Type)
	config.CreateCacheRoot(path.Join(impl.cfg.GetCacheRoot(), req.Type))
	if err := action.Init(); err != nil {
		return fmt.Errorf("ddns: service Init() failed: %w", err)
	}
	err = validator.ValidateServiceConfig(entry, impl.reg.GetServiceConfigSchema(req.Type), idx)
	if err != nil {
		return fmt.Errorf("ddns: service config validation failed: %w", err)
	}
	for _, hook := range req.OnStart {
		desc := fmt.Sprintf("Service #%d", idx)
		impl.log.Debugf("ddns: running onStart for %s", desc)
		impl.compileAndSend(*hook, desc, nil, "onStart")
	}
	resp, err := action.Execute(entry, ip)
	impl.processor.Process(resp, err, req)
	return nil
}

func (impl *defaultImpl) compileAndSend(hook webhooks.Request, name string, details map[string]any, hookType string) {
	compiled, err := impl.hooks.Compile(hook, name, details)
	if err != nil {
		impl.log.WithScope(wlog.Fields{"err": err.Error()}).Errorf("%s hook complile failed", hookType)
	} else {
		impl.hooks.Send(compiled)
	}
}
