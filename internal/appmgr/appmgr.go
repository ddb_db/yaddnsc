/*
yaddnsc
Copyright (C) 2023  Derek Battams <derek@battams.ca>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package appmgr

import (
	"encoding/json"
	"fmt"

	"github.com/vargspjut/wlog"
	"gitlab.com/ddb_db/yaddnsc/internal/config"
	"gitlab.com/ddb_db/yaddnsc/internal/registry"
	"gitlab.com/ddb_db/yaddnsc/internal/validator"
	"gitlab.com/ddb_db/yaddnsc/internal/webhooks"
)

//TODO Refactor this into multiple single purpose packages

type Provider interface {
	PrepBatch() Batch
}

type ResponseProcessor interface {
	Process(resp []byte, err error, req Request)
}

type Request struct {
	Type       string
	Config     map[string]any
	OnStart    []*webhooks.Request
	OnSuccess  []*webhooks.Request
	OnChange   []*webhooks.Request
	OnNoChange []*webhooks.Request
	OnError    []*webhooks.Request
}

type ResponseEntry struct {
	Details     map[string]any `json:"details"`
	Description string         `json:"description"`
	Status      string         `json:"status"`
}

type Response []ResponseEntry
type Batch []Request

type ProviderOption func(*defaultProvider)

func WithConfig(cfg config.Settings) ProviderOption {
	return func(impl *defaultProvider) {
		impl.cfg = cfg
	}
}

func NewProvider(opts ...ProviderOption) Provider {
	impl := defaultProvider{}
	for _, o := range opts {
		o(&impl)
	}
	if impl.cfg == nil {
		impl.cfg = config.Get()
	}
	return &impl
}

func NewResponseProcessor(opts ...ResponseProcessorOption) ResponseProcessor {
	p := &defaultResponseProcessor{}
	for _, o := range opts {
		o(p)
	}
	if p.cfg == nil {
		p.cfg = config.Get()
	}
	if p.reg == nil {
		p.reg = registry.New(registry.WithConfig(p.cfg))
	}
	if p.hooks == nil {
		p.hooks = webhooks.New(webhooks.WithConfig(p.cfg))
	}
	return p
}

type defaultProvider struct {
	cfg config.Settings
}

func (impl *defaultProvider) PrepBatch() Batch {
	var batch Batch
	if impl.cfg.GetConfigFile() != "" {
		batch = parseServiceConfig(impl.cfg.GetServicesConfig())
	} else {
		batch = make(Batch, 0)
	}
	return batch
}

func parseServiceConfig(data []byte) Batch {
	batch := make(Batch, 0)
	if err := json.Unmarshal(data, &batch); err != nil {
		panic(err)
	}
	return batch
}

type ResponseProcessorOption func(*defaultResponseProcessor)

func WithRegistry(reg registry.Registry) ResponseProcessorOption {
	return func(impl *defaultResponseProcessor) {
		impl.reg = reg
	}
}

func WithSettings(cfg config.Settings) ResponseProcessorOption {
	return func(impl *defaultResponseProcessor) {
		impl.cfg = cfg
	}
}

func WithWebHooks(hooks webhooks.Service) ResponseProcessorOption {
	return func(impl *defaultResponseProcessor) {
		impl.hooks = hooks
	}
}

type defaultResponseProcessor struct {
	cfg   config.Settings
	reg   registry.Registry
	hooks webhooks.Service
}

func (impl *defaultResponseProcessor) Process(rawResp []byte, respErr error, req Request) {
	if respErr != nil {
		wlog.WithScope(wlog.Fields{"err": respErr.Error(), "type": req.Type}).Error("request failed")
		return
	}
	if err := validator.ValidateServiceResponse(rawResp); err != nil {
		wlog.Errorf("Received response:\n%s\n", string(rawResp))
		wlog.WithScope(wlog.Fields{"err": err.Error(), "type": req.Type}).Error("response failed validation")
		return
	}

	resp := Response{}
	if err := json.Unmarshal(rawResp, &resp); err != nil {
		wlog.WithScope(wlog.Fields{"err": err.Error(), "type": req.Type, "resp": string(rawResp)}).Error("resp unmarshal failed")
		return
	}

	for _, r := range resp {
		log := wlog.WithScope(wlog.Fields{"status": r.Status, "details": r.Details, "type": req.Type, "desc": r.Description})

		if err := impl.validateResponseVars(r.Details, req.Type); err != nil {
			log.Error("service response is invalid; webhooks skipped")
			continue
		}

		if r.Status == "Err" {
			log.Error("appmgr: entry failed")
			for _, hook := range req.OnError {
				impl.compileAndSend(*hook, r.Description, r.Details, "onError", log)
			}
			continue
		}

		if r.Status == "Ok" {
			log.Info("appmgr: entry updated")
			for _, hook := range req.OnChange {
				impl.compileAndSend(*hook, r.Description, r.Details, "onChange", log)
			}
		} else {
			log.Debug("appmgr: entry processed; no change")
			for _, hook := range req.OnNoChange {
				impl.compileAndSend(*hook, r.Description, r.Details, "onNoChange", log)
			}
		}
		for _, hook := range req.OnSuccess {
			impl.compileAndSend(*hook, r.Description, r.Details, "onSuccess", log)
		}
	}
}

func (impl *defaultResponseProcessor) validateResponseVars(vars map[string]any, name string) error {
	enc, err := json.Marshal(vars)
	if err != nil {
		wlog.Errorf("Received response:\n%s\n", string(enc))
		return fmt.Errorf("vars marshal failed: %w", err)
	}

	if err := validator.ValidateServiceVariables(enc, impl.reg.GetServiceResponseSchema(name), name); err != nil {
		wlog.Errorf("Received response:\n%s\n", string(enc))
		wlog.WithScope(wlog.Fields{"err": err.Error(), "type": name}).Errorf("response variables do not conform to schema")
		return fmt.Errorf("appmgr: vars response does not conform to schema: %w", err)
	}
	return nil
}

func (impl *defaultResponseProcessor) compileAndSend(hook webhooks.Request, name string, details map[string]any, hookType string, log wlog.Logger) {
	compiled, err := impl.hooks.Compile(hook, name, details)
	if err != nil {
		log.WithScope(wlog.Fields{"err": err.Error()}).Errorf("%s hook complile failed", hookType)
	} else {
		impl.hooks.Send(compiled)
	}
}
