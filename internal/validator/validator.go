/*
yaddnsc
Copyright (C) 2023  Derek Battams <derek@battams.ca>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package validator

import (
	"bytes"
	_ "embed"
	"encoding/json"
	"fmt"
	"io"
	"strings"

	"github.com/santhosh-tekuri/jsonschema/v5"
)

func ValidateServiceConfig(input []byte, schema []byte, idx int) error {
	s := mustBuildSchema(fmt.Sprintf("idx=%d", idx), bytes.NewReader(schema))

	var blob any
	err := json.Unmarshal(input, &blob)
	if err != nil {
		return fmt.Errorf("validator: service input unmarshal failed: %w", err)
	}

	err = s.Validate(blob)
	if err != nil {
		return fmt.Errorf("validator: invalid service configuration: %w", err)
	}
	return nil
}

func ValidateServiceVariables(input []byte, schema []byte, name string) error {
	s := mustBuildSchema(name, bytes.NewReader(schema))

	var blob any
	err := json.Unmarshal(input, &blob)
	if err != nil {
		return fmt.Errorf("validator: service[%s] input unmarshal failed: %w", name, err)
	}

	err = s.Validate(blob)
	if err != nil {
		return fmt.Errorf("validator: service[%s] response invalid: %w", name, err)
	}
	return nil
}

//go:embed "resp-schema.json"
var svcRespSchema string

func GetServiceResponseSchema() string {
	return svcRespSchema
}

func ValidateServiceResponse(input []byte) error {
	s := mustBuildSchema("resp", strings.NewReader(svcRespSchema))

	var blob any
	err := json.Unmarshal(input, &blob)
	if err != nil {
		return fmt.Errorf("validator: resp input unmarshal failed: %w", err)
	}

	err = s.Validate(blob)
	if err != nil {
		return fmt.Errorf("validator: invalid service response: %w", err)
	}
	return nil
}

//go:embed "config-schema.json"
var appCfgSchema string

func GetAppConfigSchema() string {
	return appCfgSchema
}

func ValidateAppConfig(input []byte) error {
	s := mustBuildSchema("config", strings.NewReader(appCfgSchema))

	var blob any
	err := json.Unmarshal(input, &blob)
	if err != nil {
		return fmt.Errorf("validator: app config input unmarshal failed: %w", err)
	}

	err = s.Validate(blob)
	if err != nil {
		return fmt.Errorf("validator: invalid app config configuration: %w", err)
	}
	return nil
}

//go:embed "svc-schema.json"
var svcSchema string

func GetServiceSchema() string {
	return svcSchema
}

func ValidateServiceSchema(input []byte) error {
	s := mustBuildSchema("svc", strings.NewReader(svcSchema))

	var blob any
	err := json.Unmarshal(input, &blob)
	if err != nil {
		return fmt.Errorf("validator: svc input unmarshal failed: %w", err)
	}

	err = s.Validate(blob)
	if err != nil {
		return fmt.Errorf("validator: invalid svc schema configuration: %w", err)
	}
	return nil
}

func mustBuildSchema(name string, schema io.Reader) *jsonschema.Schema {
	c := jsonschema.NewCompiler()
	c.AssertFormat = true
	c.AssertContent = true
	if err := c.AddResource(name, schema); err != nil {
		panic(err)
	}
	return c.MustCompile(name)
}
