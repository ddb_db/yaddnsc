/*
yaddnsc
Copyright (C) 2023  Derek Battams <derek@battams.ca>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package interfaces

import (
	"net"
)

func GetAll() []string {
	all, err := net.Interfaces()
	if err != nil {
		panic(err)
	}
	result := make([]string, len(all))
	for n, i := range all {
		result[n] = i.Name
	}
	return result
}

func GetInterfaceIPs(name string) ([]net.IP, error) {
	iface, err := findInterfaceByName(name)
	if err != nil {
		return nil, err
	}

	result := make([]net.IP, 2)
	addrs, err := iface.Addrs()
	if err != nil {
		return nil, err
	}

	found4, found6 := false, false
	for _, a := range addrs {
		ip := a.(*net.IPNet).IP
		if ip == nil {
			continue
		}
		if ip.To4() != nil {
			if found4 {
				continue
			}
			result[0] = ip
			if !ip.IsPrivate() && ip.IsGlobalUnicast() {
				found4 = true
			}
		} else {
			if found6 {
				continue
			}
			result[1] = ip
			if !ip.IsPrivate() && ip.IsGlobalUnicast() {
				found6 = true
			}
		}
	}
	return filter(result), nil
}

func FilterOutIPv4(input []net.IP) []net.IP {
	result := make([]net.IP, 0)
	for _, i := range input {
		if i.To4() == nil {
			result = append(result, i)
		}
	}
	return result
}

func FilterOutIPv6(input []net.IP) []net.IP {
	result := make([]net.IP, 0)
	for _, i := range input {
		if i.To4() != nil {
			result = append(result, i)
		}
	}
	return result
}

func filter(input []net.IP) []net.IP {
	result := make([]net.IP, 0)
	for _, i := range input {
		if i != nil {
			result = append(result, i)
		}
	}
	return result
}

func findInterfaceByName(name string) (*net.Interface, error) {
	all, err := net.Interfaces()
	if err != nil {
		return nil, err
	}
	for _, i := range all {
		if i.Name == name {
			return &i, nil
		}
	}
	return nil, nil
}
