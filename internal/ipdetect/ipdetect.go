/*
yaddnsc
Copyright (C) 2023  Derek Battams <derek@battams.ca>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package ipdetect

import (
	"fmt"
	"net"
	"strings"

	"github.com/go-resty/resty/v2"
	"github.com/vargspjut/wlog"
	"gitlab.com/ddb_db/yaddnsc/internal/config"
	"gitlab.com/ddb_db/yaddnsc/internal/dbg"
	"gitlab.com/ddb_db/yaddnsc/internal/httputils"
)

type Detector interface {
	GetIPAddress() ([]net.IP, error)
}

func New(opts ...Option) Detector {
	impl := &defaultImpl{}
	for _, o := range opts {
		o(impl)
	}
	if impl.cfg == nil {
		impl.cfg = config.Get()
	}
	if impl.utils == nil {
		impl.utils = httputils.New(httputils.WithConfig(impl.cfg))
	}

	http := resty.New()
	http.SetHeader("Accept", "text/plain")
	http.SetHeader("User-Agent", dbg.GetUserAgent())
	if wlog.GetLogLevel() == wlog.Dbg && impl.cfg.IsLogHTTP() {
		http.EnableTrace()
	}
	impl.http = http
	return impl
}

func getDualStackURL() string {
	return "https://myipnow.xyz/api/"
}

func getIPv4URL() string {
	return "https://ipv4.myipnow.xyz/api/"
}

func getIPv6URL() string {
	return "https://ipv6.myipnow.xyz/api/"
}

type defaultImpl struct {
	cfg   config.Settings
	http  *resty.Client
	utils httputils.HTTPUtils
}

func (impl *defaultImpl) GetIPAddress() ([]net.IP, error) {
	cfg := config.Get()
	result := make([]net.IP, 0)
	if !cfg.IsForceIPv4() && !cfg.IsForceIPv6() {
		ip, err := impl.fetchIPAddress(getDualStackURL())
		if err != nil {
			return nil, err
		}
		result = append(result, ip)
		return result, nil
	}
	if cfg.IsForceIPv4() {
		ip, err := impl.fetchIPAddress(getIPv4URL())
		if err != nil {
			return nil, err
		}
		result = append(result, ip)
	}
	if cfg.IsForceIPv6() {
		ip, err := impl.fetchIPAddress(getIPv6URL())
		if err != nil {
			return nil, err
		}
		result = append(result, ip)
	}
	return result, nil
}

func (impl *defaultImpl) fetchIPAddress(url string) (net.IP, error) {
	resp, err := impl.http.R().Get(url)
	if err != nil {
		return nil, fmt.Errorf("ipdetect: api failure: %w", err)
	}
	defer impl.utils.LogRestyTrace(resp)
	if resp.IsError() {
		return nil, fmt.Errorf("ipdetect: api error[%d]", resp.StatusCode())
	}
	ip := parseResponse(string(resp.Body()))
	return ip, nil
}

func parseResponse(data string) net.IP {
	wlog.Debugf("ipdetect: raw response: %s", data)
	fields := strings.Split(data, ",")
	if len(fields) != 6 {
		panic(fmt.Errorf("ipdetect: unexpected response: %s", data))
	}
	ip := net.ParseIP(fields[1])
	wlog.Debugf("ipdetect: extracted IP: %s", fields[1])
	if ip == nil {
		panic(fmt.Errorf("ipdetect: invalid IP address: %s", fields[1]))
	}
	return ip
}
