/*
yaddnsc
Copyright (C) 2023  Derek Battams <derek@battams.ca>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package ipdetect

import (
	"fmt"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIPParserResponsePanicsWhenResponseIsInvalid(t *testing.T) {
	var tests = []struct {
		input string
		desc  string
	}{
		{"", "empty input"},
		{"foo,bar", "too few fields"},
		{"foo,,,,,,", "too many fields"},
	}

	for _, tc := range tests {
		t.Run(tc.desc, func(t *testing.T) {
			assert.PanicsWithError(t, fmt.Sprintf("ipdetect: unexpected response: %s", tc.input), func() {
				parseResponse(tc.input)
			})
		})
	}
}

func TestParseResponsePanicsWhenIPAddrIsInvalid(t *testing.T) {
	var tests = []struct {
		input string
		desc  string
	}{
		{"x,foobar,,,,", "garbage data"},
		{"x,300.0.0.1,,,,", "invalid IPv4 address"},
		{"x,vbc:3321:ab::,,,,", "invalid IPv6 address"},
	}

	for _, tc := range tests {
		t.Run(tc.desc, func(t *testing.T) {
			ip := strings.Split(tc.input, ",")[1]
			assert.PanicsWithError(t, fmt.Sprintf("ipdetect: invalid IP address: %s", ip), func() {
				parseResponse(tc.input)
			})
		})
	}
}

func TestParseResponseReturnsIPs(t *testing.T) {
	var tests = []struct {
		input string
		desc  string
	}{
		{"x,192.168.0.1,,,,", "valid IPv4 address"},
		{"x,2600:acc1:3e::,,,,", "valid IPv6 address"},
	}

	for _, tc := range tests {
		t.Run(tc.desc, func(t *testing.T) {
			ip := strings.Split(tc.input, ",")[1]
			assert.Equal(t, ip, parseResponse(tc.input).String())
		})
	}
}
