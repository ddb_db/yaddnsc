/*
yaddnsc
Copyright (C) 2023  Derek Battams <derek@battams.ca>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package ipdetect

import (
	"github.com/vargspjut/wlog"
	"gitlab.com/ddb_db/yaddnsc/internal/config"
	"gitlab.com/ddb_db/yaddnsc/internal/httputils"
)

type Option func(*defaultImpl)

func WithConfig(cfg config.Settings) Option {
	return func(impl *defaultImpl) {
		wlog.Debug("ipdetect: custom config")
		impl.cfg = cfg
	}
}

func WithHTTPUtils(utils httputils.HTTPUtils) Option {
	return func(impl *defaultImpl) {
		wlog.Debug("ipdetect: custom HTTPUtils")
		impl.utils = utils
	}
}
