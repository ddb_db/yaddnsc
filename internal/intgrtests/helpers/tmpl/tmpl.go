/*
yaddnsc
Copyright (C) 2023  Derek Battams <derek@battams.ca>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package tmpl

import (
	"fmt"
	"os"
	"path"
	"strings"
	"text/template"
)

func Compile(file string, outputRoot string) string {
	b, err := os.ReadFile(file)
	if err != nil {
		panic(err)
	}
	t, err := template.New(file).Parse(string(b))
	if err != nil {
		panic(err)
	}
	out := fmt.Sprintf("*_%s", path.Base(file))
	f, err := os.CreateTemp("", out)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	m := envToMap()
	m["YADDNSC_OUTPUT_ROOT"] = outputRoot
	if err := t.Execute(f, m); err != nil {
		panic(err)
	}
	return f.Name()
}

func envToMap() map[string]string {
	env := make(map[string]string)
	for _, e := range os.Environ() {
		p := strings.SplitN(e, "=", 2)
		env[p[0]] = p[1]
	}
	return env
}
