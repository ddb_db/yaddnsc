/*
yaddnsc
Copyright (C) 2023  Derek Battams <derek@battams.ca>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package cfhelper

import (
	"context"
	"fmt"
	"os"

	"github.com/cloudflare/cloudflare-go"
)

const envToken = "CF_API_TOKEN"
const envZoneID = "CF_ZONEID"

var api *cloudflare.API
var token string
var zoneID string

func init() {
	zoneID = os.Getenv(envZoneID)
	token = os.Getenv(envToken)
	cf, err := cloudflare.NewWithAPIToken(token)
	if err != nil {
		panic(err)
	}
	api = cf
}

func ResetDNS(host string, ip string) {
	results, info, err := api.ListDNSRecords(context.Background(), cloudflare.ZoneIdentifier(zoneID), cloudflare.ListDNSRecordsParams{
		Type: "A",
		Name: host,
	})
	if err != nil {
		panic(err)
	}
	if info.Count != 1 {
		panic(fmt.Errorf("expecting 1 DNS record match, got %d", info.Count))
	}
	result := results[0]
	if result.Content != ip {
		if r, err := api.UpdateDNSRecord(context.Background(), cloudflare.ZoneIdentifier(zoneID), cloudflare.UpdateDNSRecordParams{
			ID:      result.ID,
			Content: ip,
			Type:    result.Type,
		}); err != nil {
			panic(err)
		} else if r.Content != ip {
			panic(fmt.Errorf("DNS reset failed"))
		}
	}
}
