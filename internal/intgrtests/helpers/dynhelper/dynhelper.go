/*
yaddnsc
Copyright (C) 2023  Derek Battams <derek@battams.ca>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package dynhelper

import (
	"fmt"
	"os"

	"github.com/go-resty/resty/v2"
)

const envServiceHost = "DYN_SERVICE_HOST"
const envLogin = "DYN_LOGIN"
const envPassword = "DYN_PASSWORD"
const envHost = "DYN_HOST"

var api *resty.Client
var serviceHost string
var login string
var password string
var DefaultHost string

func init() {
	var ok bool
	serviceHost, ok = os.LookupEnv(envServiceHost)
	if !ok {
		panic(fmt.Errorf("%s env var must be defined", envServiceHost))
	}

	login, ok = os.LookupEnv(envLogin)
	if !ok {
		panic(fmt.Errorf("%s env var must be defined", envLogin))
	}

	password, ok = os.LookupEnv(envPassword)
	if !ok {
		panic(fmt.Errorf("%s env var must be defined", envPassword))
	}

	DefaultHost, ok = os.LookupEnv(envHost)
	if !ok {
		panic(fmt.Errorf("%s env var must be defined", envHost))
	}

	http := resty.New()
	http.SetBaseURL(fmt.Sprintf("https://%s", serviceHost))
	http.SetBasicAuth(login, password)
	api = http
}

func ResetDNS(host string, ip string) {
	req := api.R()
	req.SetQueryParam("hostname", host)
	req.SetQueryParam("myip", ip)
	resp, err := req.Get("/v3/update")
	if err != nil {
		panic(err)
	}
	if resp.StatusCode() != 200 {
		panic(fmt.Errorf("dns reset failed: %d", resp.StatusCode()))
	}
	code := string(resp.Body())
	if code != "good" && code != "nochg" {
		panic(fmt.Errorf("dns reset failed: %s", code))
	}
}
