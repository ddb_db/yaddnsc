/*
yaddnsc
Copyright (C) 2023  Derek Battams <derek@battams.ca>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package intgrtests

import (
	"fmt"
	"os"
)

const DefaultIP = "100.0.0.0"
const UnknownIP = "0.0.0.0"

func MkCacheRoot() string {
	root, err := os.MkdirTemp("", "yaddnsc_intgrtests_*")
	if err != nil {
		panic(err)
	}
	return root
}

func MkOutputRoot() string {
	root, ok := os.LookupEnv("CI_PROJECT_DIR")
	if !ok {
		var err error
		root, err = os.UserHomeDir()
		if err != nil {
			panic(err)
		}
		root = fmt.Sprintf("%s/.yaddnsc/intgrtests", root)
	} else {
		root = fmt.Sprintf("%s/build/intgrtests", root)
	}
	if err := os.MkdirAll(root, 0755); err != nil {
		panic(err)
	}
	root, err := os.MkdirTemp(root, "*")
	if err != nil {
		panic(err)
	}
	return root
}
