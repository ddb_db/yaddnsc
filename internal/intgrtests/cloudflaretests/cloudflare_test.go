/*
yaddnsc
Copyright (C) 2023  Derek Battams <derek@battams.ca>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package cloudflaretests

import (
	_ "embed"
	"encoding/json"
	"fmt"
	"net"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/vargspjut/wlog"
	yaddnsccf "gitlab.com/ddb_db/yaddnsc-cloudflare"
	"gitlab.com/ddb_db/yaddnsc/internal/appmgr"
	"gitlab.com/ddb_db/yaddnsc/internal/config"
	"gitlab.com/ddb_db/yaddnsc/internal/ddns"
	"gitlab.com/ddb_db/yaddnsc/internal/intgrtests"
	"gitlab.com/ddb_db/yaddnsc/internal/intgrtests/helpers/cfhelper"
	"gitlab.com/ddb_db/yaddnsc/internal/intgrtests/helpers/tmpl"
	"gitlab.com/ddb_db/yaddnsc/internal/registry"
	"gitlab.com/ddb_db/yaddnsc/internal/validator"
	"gitlab.com/ddb_db/yaddnsc/internal/webhooks"
)

var defaultHost string

func init() {
	var ok bool
	defaultHost, ok = os.LookupEnv("CF_HOST")
	if !ok {
		panic("CF_HOST must be defined")
	}
}

func TestNoChg(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}

	cacheRoot := intgrtests.MkCacheRoot()
	defer os.RemoveAll(cacheRoot)
	outRoot := intgrtests.MkOutputRoot() // do not nuke output root; we want to collect the artifacts
	cfhelper.ResetDNS(defaultHost, intgrtests.DefaultIP)
	cfg := &mockConfig{
		cacheRoot:  cacheRoot,
		configFile: tmpl.Compile("testdata/cloudflare.json", outRoot),
	}
	defer os.Remove(cfg.configFile)

	reg := registry.New(registry.WithConfig(cfg))
	cf := yaddnsccf.New(wlog.DefaultLogger())
	hooks := webhooks.New(webhooks.WithConfig(cfg))
	processor := appmgr.NewResponseProcessor(appmgr.WithRegistry(reg), appmgr.WithSettings(cfg))
	if err := cf.Init(); err != nil {
		panic(err)
	}
	dyndns := ddns.New(reg, processor, cfg, hooks, wlog.DefaultLogger())

	batch := appmgr.NewProvider(appmgr.WithConfig(cfg)).PrepBatch()
	assert.Equal(t, 1, len(batch))
	err := dyndns.Execute(0, batch[0], net.ParseIP(intgrtests.DefaultIP))
	if !assert.NoError(t, err) {
		panic(err)
	}

	results := fetchWebhooks(outRoot)
	assert.Equal(t, 1, len(results))
	details := results["success"]
	assert.Equal(t, "NoChg", details["status"])
	assert.Equal(t, intgrtests.DefaultIP, details["currentIP"])
	assert.Equal(t, intgrtests.DefaultIP, details["previousIP"])
	assert.Equal(t, os.Getenv("CF_HOST"), details["host"])
	assert.Equal(t, os.Getenv("CF_ZONEID"), details["zoneID"])
}

func TestUpdate(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}

	cacheRoot := intgrtests.MkCacheRoot()
	defer os.RemoveAll(cacheRoot)
	outRoot := intgrtests.MkOutputRoot()
	cfhelper.ResetDNS(defaultHost, intgrtests.DefaultIP)
	cfg := &mockConfig{
		cacheRoot:  cacheRoot,
		configFile: tmpl.Compile("testdata/cloudflare.json", outRoot),
	}
	defer os.Remove(cfg.configFile)

	reg := registry.New(registry.WithConfig(cfg))
	cf := yaddnsccf.New(wlog.DefaultLogger())
	hooks := webhooks.New(webhooks.WithConfig(cfg))
	processor := appmgr.NewResponseProcessor(appmgr.WithRegistry(reg), appmgr.WithSettings(cfg))
	if err := cf.Init(); err != nil {
		panic(err)
	}
	dyndns := ddns.New(reg, processor, cfg, hooks, wlog.DefaultLogger())

	batch := appmgr.NewProvider(appmgr.WithConfig(cfg)).PrepBatch()
	assert.Equal(t, 1, len(batch))
	newIP := "100.10.15.100"
	err := dyndns.Execute(0, batch[0], net.ParseIP(newIP))
	if !assert.NoError(t, err) {
		panic(err)
	}

	results := fetchWebhooks(outRoot)
	assert.Equal(t, 1, len(results))
	details := results["success"]
	assert.Equal(t, "Ok", details["status"])
	assert.Equal(t, newIP, details["currentIP"])
	assert.Equal(t, intgrtests.DefaultIP, details["previousIP"])
	assert.Equal(t, os.Getenv("CF_HOST"), details["host"])
	assert.Equal(t, os.Getenv("CF_ZONEID"), details["zoneID"])
}

func TestErr(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}

	cacheRoot := intgrtests.MkCacheRoot()
	defer os.RemoveAll(cacheRoot)
	outRoot := intgrtests.MkOutputRoot()
	cfhelper.ResetDNS(defaultHost, intgrtests.DefaultIP)
	cfg := &mockConfig{
		cacheRoot:  cacheRoot,
		configFile: tmpl.Compile("testdata/cloudflare.json", outRoot),
	}
	defer os.Remove(cfg.configFile)

	reg := registry.New(registry.WithConfig(cfg))
	cf := yaddnsccf.New(wlog.DefaultLogger())
	hooks := webhooks.New(webhooks.WithConfig(cfg))
	processor := appmgr.NewResponseProcessor(appmgr.WithRegistry(reg), appmgr.WithSettings(cfg))
	if err := cf.Init(); err != nil {
		panic(err)
	}
	dyndns := ddns.New(reg, processor, cfg, hooks, wlog.DefaultLogger())

	batch := appmgr.NewProvider(appmgr.WithConfig(cfg)).PrepBatch()
	assert.Equal(t, 1, len(batch))
	newIP := "300.0.0.0" // invalid IPv4 address
	err := dyndns.Execute(0, batch[0], net.ParseIP(newIP))
	if !assert.NoError(t, err) {
		panic(err)
	}

	results := fetchWebhooks(outRoot)
	assert.Equal(t, 1, len(results))
	details := results["error"]
	assert.Equal(t, "Err", details["status"])
}

//go:embed "testdata/cf-resp.json"
var invalidResp []byte

func TestNullExistingIPInResponse(t *testing.T) {
	cf := yaddnsccf.New(wlog.DefaultLogger())
	var schema map[string]any
	if err := json.Unmarshal(cf.Schema(), &schema); err != nil {
		panic(err)
	}
	respSchema, err := json.Marshal(schema["response"])
	if err != nil {
		panic(err)
	}
	result := validator.ValidateServiceVariables(invalidResp, respSchema, "test")

	assert.NotNil(t, result)
}

func fetchWebhooks(root string) map[string]map[string]any {
	result := make(map[string]map[string]any)
	for _, status := range []string{"start", "success", "error", "nochg", "update"} {
		f := fmt.Sprintf("%s/%s.txt", root, status)
		if !fileExists(f) {
			continue
		}
		data, err := os.ReadFile(f)
		if err != nil {
			panic(err)
		}
		var payload map[string]any
		if err := json.Unmarshal(data, &payload); err != nil {
			panic(err)
		}
		result[status] = payload
	}
	return result
}

func fileExists(name string) bool {
	_, err := os.Stat(name)
	return err == nil
}

type mockConfig struct {
	config.Settings
	cacheRoot  string
	configFile string
	services   []byte
}

func (cfg *mockConfig) GetCacheRoot() string {
	return cfg.cacheRoot
}

func (cfg *mockConfig) GetConfigFile() string {
	return cfg.configFile
}

func (cfg *mockConfig) GetServicesConfig() []byte {
	cfg.initJSON()
	return cfg.services
}

func (cfg *mockConfig) GetMyIP() config.IpOverrides { return []string{} }
func (cfg *mockConfig) GetInterface() string        { return "" }
func (cfg *mockConfig) GetLogLevel() string         { return "" }
func (cfg *mockConfig) GetLoopMinutes() int         { return 1 }
func (cfg *mockConfig) IsForceIPv4() bool           { return false }
func (cfg *mockConfig) IsForceIPv6() bool           { return false }
func (cfg *mockConfig) IsLogHTTP() bool             { return false }
func (cfg *mockConfig) IsLogHTTPBodies() bool       { return false }
func (cfg *mockConfig) IsLoopMode() bool            { return false }
func (cfg *mockConfig) IsSchema() bool              { return false }
func (cfg *mockConfig) IsServices() bool            { return false }
func (cfg *mockConfig) IsInterfaces() bool          { return false }
func (cfg *mockConfig) IsVersion() bool             { return false }
func (cfg *mockConfig) IsHelp() bool                { return false }
func (cfg *mockConfig) IsTerminal() bool            { return false }

func (cfg *mockConfig) initJSON() {
	if cfg.services != nil {
		return
	}
	type services struct {
		Services []any
	}
	data, err := os.ReadFile(cfg.GetConfigFile())
	if err != nil {
		panic(err)
	}
	svcs := services{}
	if err := json.Unmarshal(data, &svcs); err != nil {
		panic(err)
	}
	enc, err := json.Marshal(svcs.Services)
	if err != nil {
		panic(err)
	}
	cfg.services = enc
}
