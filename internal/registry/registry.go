/*
yaddnsc
Copyright (C) 2023  Derek Battams <derek@battams.ca>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package registry

import (
	"encoding/json"
	"fmt"
	"net"
	"path"
	"strings"

	"github.com/vargspjut/wlog"
	yaddnsccf "gitlab.com/ddb_db/yaddnsc-cloudflare"
	dyndns "gitlab.com/ddb_db/yaddnsc-dyndns"
	"gitlab.com/ddb_db/yaddnsc/internal/config"
	"gitlab.com/ddb_db/yaddnsc/internal/dbg"
	"gitlab.com/ddb_db/yaddnsc/internal/validator"
)

const (
	svcCloudflare = "cloudflare"
	svcDynDNS     = "dyndns"

	logCatForSvc = "__svc"
)

type Registry interface {
	GetService(name string) DDNSSupport
	GetServiceConfigSchema(name string) []byte
	GetServiceResponseSchema(name string) []byte
	DescribeSchemas() map[string]any
	DescribeSupportedServices() string
}

type DDNSSupport interface {
	Init() error
	Schema() []byte
	Describe() string
	Execute(blob []byte, systemIP net.IP) ([]byte, error)
}

type serviceSchemas struct {
	Config   any `json:"config"`
	Response any `json:"response"`
}

type jsonSchema map[string]serviceSchemas

type Option func(*defaultImpl)

func WithConfig(cfg config.Settings) Option {
	return func(impl *defaultImpl) {
		impl.cfg = cfg
	}
}

func New(opts ...Option) Registry {
	impl := &defaultImpl{}
	for _, o := range opts {
		o(impl)
	}
	if impl.cfg == nil {
		impl.cfg = config.Get()
	}
	impl.init()
	return impl
}

type defaultImpl struct {
	cfg        config.Settings
	serviceMap jsonSchema
}

func (impl *defaultImpl) init() {
	impl.serviceMap = make(jsonSchema)
	impl.registerSvc(svcCloudflare, impl.GetService(svcCloudflare))
	impl.registerSvc(svcDynDNS, impl.GetService(svcDynDNS))
}

func (impl *defaultImpl) registerSvc(name string, svc DDNSSupport) {
	schema, err := blobToJSONSchema(svc.Schema())
	if err != nil {
		wlog.Errorf("Service '%s' has improper schema: %s", name, err.Error())
		return
	}
	impl.serviceMap[name] = schema
}

func (impl *defaultImpl) GetService(name string) DDNSSupport {
	isHttpLog := impl.cfg.IsLogHTTP() && wlog.DefaultLogger().GetLogLevel() == wlog.Dbg
	switch name {
	case svcCloudflare:
		return yaddnsccf.New(wlog.WithScope(wlog.Fields{logCatForSvc: svcCloudflare}))
	case svcDynDNS:
		return dyndns.New(path.Join(impl.cfg.GetCacheRoot(), svcDynDNS), wlog.WithScope(wlog.Fields{logCatForSvc: svcDynDNS}), dbg.GetUserAgent(), isHttpLog)
	default:
		panic(fmt.Errorf("registry: unknown service type [%s]", name))
	}
}

func (impl *defaultImpl) GetServiceConfigSchema(name string) []byte {
	svc := impl.serviceMap[name]
	enc, err := json.Marshal(svc.Config)
	if err != nil {
		panic(err)
	}
	return enc
}

func (impl *defaultImpl) GetServiceResponseSchema(name string) []byte {
	svc := impl.serviceMap[name]
	enc, err := json.Marshal(svc.Response)
	if err != nil {
		panic(err)
	}
	return enc
}

func (impl *defaultImpl) describeServiceSchemas() jsonSchema {
	return impl.serviceMap
}

func (impl *defaultImpl) DescribeSchemas() map[string]any {
	m := make(map[string]any)
	m["config"] = decode(validator.GetAppConfigSchema())
	m["resp"] = decode(validator.GetServiceResponseSchema())
	m["service"] = decode(validator.GetServiceSchema())
	m["services"] = impl.describeServiceSchemas()
	return m
}

func decode(input string) map[string]any {
	var blob map[string]any
	err := json.Unmarshal([]byte(input), &blob)
	if err != nil {
		panic(err)
	}
	return blob
}

func (impl *defaultImpl) DescribeSupportedServices() string {
	const format = "%-32s %-55s\n"
	sb := strings.Builder{}
	sb.WriteString(fmt.Sprintf(format, "Type", "Description"))
	sb.WriteString(strings.Repeat("=", sb.Len()-1) + "\n")
	for k := range impl.serviceMap {
		sb.WriteString(fmt.Sprintf(format, k, impl.GetService(k).Describe()))
	}
	return sb.String()
}

func blobToJSONSchema(blob []byte) (serviceSchemas, error) {
	if err := validator.ValidateServiceSchema(blob); err != nil {
		return serviceSchemas{}, fmt.Errorf("registry: service schema invalid: %w", err)
	}

	schema := serviceSchemas{}
	err := json.Unmarshal(blob, &schema)
	if err != nil {
		panic(fmt.Errorf("registry: json schema unmarshal failed: %w", err))
	}
	return schema, nil
}
