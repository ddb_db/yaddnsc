/*
yaddnsc
Copyright (C) 2023  Derek Battams <derek@battams.ca>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package dbg

import (
	"fmt"
	"runtime/debug"
	"strings"
)

const YaddnscVer = "0.0.1"
const YaddnscHome = "https://gitlab.com/ddb_db/yaddnsc"

func GetBuildSHA() string {
	sha := getBuildInfo("vcs.revision")
	if sha == "" {
		return sha
	}
	return sha[0:8]
}

func GetVersionString(withSHA bool) string {
	ver := YaddnscVer
	isDev := getBuildInfo("vcs.modified")
	if strings.ToLower(isDev) != "false" {
		ver = ver + "-dev"
	}
	if withSHA {
		sha := GetBuildSHA()
		if sha != "" {
			ver = ver + " (" + sha + ")"
		}
	}
	return fmt.Sprint(ver)
}

func GetUserAgent() string {
	ua := fmt.Sprintf("yaddnsc/%s (", GetVersionString(false))
	os := getBuildInfo("GOOS")
	arch := getBuildInfo("GOARCH")
	sha := GetBuildSHA()
	if os != "" && arch != "" {
		ua = fmt.Sprintf("%s%s/%s; ", ua, os, arch)
	}
	if sha != "" {
		ua = fmt.Sprintf("%sbld=%s; ", ua, sha)
	}
	ua = fmt.Sprintf("%s+%s)", ua, YaddnscHome)
	return ua
}

func getBuildInfo(key string) string {
	if info, ok := debug.ReadBuildInfo(); ok {
		for _, s := range info.Settings {
			if s.Key == key {
				return s.Value
			}
		}
	}
	return ""
}
