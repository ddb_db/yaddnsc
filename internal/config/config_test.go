/*
yaddnsc
Copyright (C) 2023  Derek Battams <derek@battams.ca>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package config

import (
	"fmt"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/vargspjut/wlog"
)

func TestParseLogLevel(t *testing.T) {
	var tests = []struct {
		input    string
		expected wlog.LogLevel
	}{
		{"debug", wlog.Dbg},
		{"info", wlog.Nfo},
		{"warn", wlog.Wrn},
		{"error", wlog.Err},
		{"fatal", wlog.Ftl},
		{"unknown", wlog.Nfo},
	}

	for _, tc := range tests {
		perms := permutations(tc.input)
		for _, p := range perms {
			t.Run(fmt.Sprintf("'%s' returns %+v", p, tc.expected), func(t *testing.T) {
				lvl := parseLogLevel(p)
				assert.Equal(t, tc.expected, lvl)
			})
		}
	}
}

func permutations(input string) []string {
	return dfs(input, "")
}

func dfs(input, output string) []string {
	result := make([]string, 0)
	if len(input) == 0 {
		result = append(result, output)
		return result
	}
	l := strings.ToLower(input[0:1])
	u := strings.ToUpper(input[0:1])
	result = append(result, dfs(input[1:], output+l)...)
	result = append(result, dfs(input[1:], output+u)...)
	return result
}
