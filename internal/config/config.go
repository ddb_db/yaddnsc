/*
yaddnsc
Copyright (C) 2023  Derek Battams <derek@battams.ca>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package config

import (
	"encoding/json"
	"errors"
	"flag"
	"net"
	"os"
	"path"
	"strings"

	"github.com/davecgh/go-spew/spew"
	"github.com/vargspjut/wlog"
	"gitlab.com/ddb_db/yaddnsc/internal/validator"
)

type Settings interface {
	GetConfigFile() string
	GetCacheRoot() string
	GetServicesConfig() []byte
	GetMyIP() IpOverrides
	GetInterface() string
	GetLogLevel() string
	GetLoopMinutes() int
	IsForceIPv4() bool
	IsForceIPv6() bool
	IsLogHTTP() bool
	IsLogHTTPBodies() bool
	IsLoopMode() bool
	IsSchema() bool
	IsServices() bool
	IsInterfaces() bool
	IsVersion() bool
	IsHelp() bool
	IsTerminal() bool
}

type IpOverrides []string

func (ips IpOverrides) String() string {
	return strings.Join(ips, ",")
}

type values struct {
	configFile         string
	CacheRoot          string      `json:"cacheRoot"`
	MyIP               IpOverrides `json:"myip"`
	Iface              string      `json:"interface"`
	LoopMinutes        int         `json:"loopMinutes"`
	LogLevel           string      `json:"logLevel"`
	IsForceIPv4Val     bool        `json:"forceIPv4"`
	IsForceIPv6Val     bool        `json:"forceIPv6"`
	IsLogHTTPVal       bool        `json:"logHttp"`
	IsLogHTTPBodiesVal bool        `json:"logHttpBodies"`
	IsLoopModeVal      bool        `json:"loopMode"`
	isSchemaVal        bool
	isServicesVal      bool
	isInterfacesVal    bool
	isVersionVal       bool
	isHelpVal          bool

	Services       []any `json:"services"`
	servicesConfig []byte
}

func (v *values) IsSchema() bool {
	return v.isSchemaVal
}

func (v *values) IsServices() bool {
	return v.isServicesVal
}

func (v *values) IsInterfaces() bool {
	return v.isInterfacesVal
}

func (v *values) IsHelp() bool {
	return v.isHelpVal
}

func (v *values) IsVersion() bool {
	return v.isVersionVal
}

func (v *values) GetCacheRoot() string {
	return v.CacheRoot
}

func (v *values) GetLoopMinutes() int {
	return v.LoopMinutes
}

func (v *values) IsLoopMode() bool {
	return v.IsLoopModeVal
}

func (v *values) GetServicesConfig() []byte {
	if v.servicesConfig == nil {
		enc, err := json.Marshal(v.Services)
		if err != nil {
			panic(err)
		}
		v.servicesConfig = enc
	}
	return v.servicesConfig
}

func (v *values) GetInterface() string {
	return v.Iface
}

func (v *values) GetConfigFile() string {
	return v.configFile
}

func (v *values) IsLogHTTPBodies() bool {
	return v.IsLogHTTPBodiesVal
}

func (v *values) GetLogLevel() string {
	return v.LogLevel
}

func (v *values) IsLogHTTP() bool {
	return v.IsLogHTTPVal
}

func (v *values) GetMyIP() IpOverrides {
	return v.MyIP
}

func (v *values) IsForceIPv4() bool {
	return v.IsForceIPv4Val
}

func (v *values) IsForceIPv6() bool {
	return v.IsForceIPv6Val
}

var vals *values

func Init() Settings {
	if vals == nil {
		defaultCacheRoot, err := os.UserHomeDir()
		if err != nil {
			panic(err)
		}
		vals = &values{
			CacheRoot:   path.Join(defaultCacheRoot, ".yaddnsc"),
			LoopMinutes: 15,
		}
		flag.StringVar(&vals.configFile, "config", "yaddnsc.json", "config file to load for execution")
		flag.BoolVar(&vals.isSchemaVal, "schema", false, "Describe supported DDNS services and their configuration via JSON schema and exit immediately")
		flag.BoolVar(&vals.isServicesVal, "services", false, "Describe supported DDNS services in human friendly format and exit immediately")
		flag.BoolVar(&vals.isInterfacesVal, "interfaces", false, "List names of all known network interfaces that can be used as IP source and exit immediately")
		flag.BoolVar(&vals.isVersionVal, "version", false, "Show version info and exit immediately")
		flag.BoolVar(&vals.isHelpVal, "help", false, "Show usage info and exit immediately")
		flag.Parse()
		if !vals.IsTerminal() {
			parseConfigOptions(vals)
		}
		wlog.DefaultLogger().SetLogLevel(parseLogLevel(vals.LogLevel))
		wlog.Debug(spew.Sdump(vals))
	}
	return vals
}

func Get() Settings {
	return Init()
}

func (vals *values) IsTerminal() bool {
	return vals.IsSchema() ||
		vals.IsServices() ||
		vals.IsInterfaces() ||
		vals.IsVersion() ||
		vals.IsHelp()
}

func parseConfigOptions(vals *values) {
	enc, err := os.ReadFile(vals.configFile)
	if err != nil {
		panic(err)
	}
	if err := validator.ValidateAppConfig(enc); err != nil {
		panic(err)
	}
	if err := json.Unmarshal(enc, &vals); err != nil {
		panic(err)
	}
	validateMyIP(vals.MyIP)
}

func CreateCacheRoot(dir string) string {
	info, err := os.Stat(dir)
	if os.IsNotExist(err) {
		if err := os.MkdirAll(dir, 0755); err != nil {
			panic(err)
		}
		return dir
	}
	if err != nil {
		panic(err)
	}
	if !info.IsDir() {
		panic(errors.New("cache root exists but is not a directory"))
	}
	return dir
}

func validateMyIP(myip IpOverrides) {
	have4, have6 := false, false
	// NOTE: The json schema already ensures these values are valid IP addresses; we're just making sure there is only one v4 and one v6
	for _, s := range myip {
		ip := net.ParseIP(s)
		if ip == nil {
			panic("invalid ip") // this can't happen due to json schema enforcement
		}
		ip4 := ip.To4()
		if ip4 != nil {
			if have4 {
				panic("only one IPv4 address allowed in myip")
			}
			have4 = true
		} else {
			if have6 {
				panic("only one IPv6 address allowed in myip")
			}
			have6 = true
		}
	}
}

func parseLogLevel(lvl string) wlog.LogLevel {
	switch strings.ToUpper(lvl) {
	case "DEBUG":
		return wlog.Dbg
	case "INFO":
		return wlog.Nfo
	case "WARN":
		return wlog.Wrn
	case "ERROR":
		return wlog.Err
	case "FATAL":
		return wlog.Ftl
	default:
		return wlog.Nfo
	}
}
