module gitlab.com/ddb_db/yaddnsc

go 1.20

require github.com/cloudflare/cloudflare-go v0.70.0

require (
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/hashicorp/go-retryablehttp v0.7.4 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/text v0.10.0 // indirect
	golang.org/x/time v0.3.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/go-resty/resty/v2 v2.7.0
	github.com/santhosh-tekuri/jsonschema/v5 v5.3.0
	github.com/stretchr/testify v1.8.4
	github.com/vargspjut/wlog v1.0.11
	gitlab.com/ddb_db/yaddnsc-cloudflare v0.1.4
	gitlab.com/ddb_db/yaddnsc-dyndns v0.0.4
	golang.org/x/net v0.11.0 // indirect
)
