/*
yaddnsc
Copyright (C) 2023  Derek Battams <derek@battams.ca>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"net"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/vargspjut/wlog"
	"gitlab.com/ddb_db/yaddnsc/internal/appmgr"
	"gitlab.com/ddb_db/yaddnsc/internal/config"
	"gitlab.com/ddb_db/yaddnsc/internal/dbg"
	"gitlab.com/ddb_db/yaddnsc/internal/ddns"
	"gitlab.com/ddb_db/yaddnsc/internal/interfaces"
	"gitlab.com/ddb_db/yaddnsc/internal/ipdetect"
	"gitlab.com/ddb_db/yaddnsc/internal/registry"
	"gitlab.com/ddb_db/yaddnsc/internal/webhooks"
)

func main() {
	cfg := config.Init()
	checkTerminalOptions()
	batch := appmgr.NewProvider().PrepBatch()
	processor := appmgr.NewResponseProcessor()
	hooks := webhooks.New(webhooks.WithConfig(cfg))
	dyndns := ddns.New(registry.New(), processor, cfg, hooks, wlog.DefaultLogger())
	for {
		myIPs := mustGetMyIP()
		for _, ip := range myIPs {
			for i, r := range batch {
				if err := dyndns.Execute(i, r, ip); err != nil {
					panic(err)
				}
			}
		}
		if !cfg.IsLoopMode() {
			break
		}
		if err := sleep(cfg.GetLoopMinutes()); err != nil {
			break
		}
	}
}

func sleep(mins int) error {
	t := time.NewTimer(time.Minute * time.Duration(mins))
	s := make(chan os.Signal, 1)
	defer func() {
		t.Stop()
		signal.Stop(s)
		close(s)
	}()
	signal.Notify(s, syscall.SIGINT, syscall.SIGTERM)
	wlog.WithScope(wlog.Fields{"mins": mins}).Debug("sleeping")
	select {
	case <-t.C:
		wlog.WithScope(wlog.Fields{"mins": mins}).Debug("awake")
		return nil
	case r := <-s:
		wlog.WithScope(wlog.Fields{"sig": r}).Info("signal received")
		return fmt.Errorf("signal received: %v", r)
	}
}

func mustGetMyIP() []net.IP {
	cfg := config.Get()
	myIP := cfg.GetMyIP()
	if len(myIP) > 0 {
		ips := make([]net.IP, len(myIP))
		for i, ip := range myIP {
			parsed := net.ParseIP(ip)
			if parsed == nil {
				panic("-myip is not a valid IP address")
			}
			ips[i] = parsed
		}
		return ips
	}

	myIface := cfg.GetInterface()
	if myIface != "" {
		ips, err := interfaces.GetInterfaceIPs(myIface)
		if err != nil {
			panic(err)
		}
		if cfg.IsForceIPv4() && !cfg.IsForceIPv6() {
			ips = interfaces.FilterOutIPv6(ips)
		}
		if cfg.IsForceIPv6() && !cfg.IsForceIPv4() {
			ips = interfaces.FilterOutIPv4(ips)
		}
		return ips
	}

	ips, err := ipdetect.New().GetIPAddress()
	if err != nil {
		panic(err)
	}
	return ips
}

func checkTerminalOptions() {
	cfg := config.Get()
	reg := registry.New()

	if cfg.IsVersion() {
		fmt.Printf("yaddnsc %s\n\n", dbg.GetVersionString(true))
		fmt.Printf("%s\n", dbg.YaddnscHome)
		fmt.Println("(C) 2023- ddb")
		fmt.Println("Licensed under the GNU GPLv3 license -- All rights reserved.")
		os.Exit(1)
	}
	if cfg.IsHelp() {
		flag.Usage()
		os.Exit(1)
	}

	if cfg.IsSchema() {
		enc, err := json.MarshalIndent(reg.DescribeSchemas(), "", "  ")
		if err != nil {
			panic(err)
		}
		fmt.Println(string(enc))
		os.Exit(0)
	}

	if cfg.IsServices() {
		fmt.Println(reg.DescribeSupportedServices())
		os.Exit(0)
	}

	if cfg.IsInterfaces() {
		fmt.Println(strings.Join(interfaces.GetAll(), ","))
		os.Exit(0)
	}
}
