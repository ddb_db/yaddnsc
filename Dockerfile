FROM alpine:latest
RUN adduser -u 1099 -D yaddnsc && \
    apk add -U --no-cache ca-certificates

FROM scratch
COPY --from=0 --chmod=644 /etc/passwd /etc/
COPY --from=0 --chmod=644 /etc/group /etc/
COPY --from=0 --chmod=644 /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --chmod=755 linux-amd64/yaddnsc /usr/local/bin/
VOLUME /config
USER yaddnsc
ENTRYPOINT ["/usr/local/bin/yaddnsc"]
